import redis
class REDIS():
    def __init__(self,host,port):
        self.host = host
        self.port = port
        self.redisdb = redis.Redis(host=self.host,port=self.port,db=0)
    
    def post(self,palabra,definicion):
        data = {'palabra':palabra,'definicion':definicion}
        self.redisdb.hmset('Palabras:'+palabra,data)
        print('La palabra se añadido exitosamente')

    def getDefinicion(self,palabra):
        resultado = self.redisdb.hget('Palabras:'+palabra,'definicion')
        try:
            print('Definicion:'+resultado.decode('utf-8'))
        except:
            print('Definicion:'+resultado)

    def getAllPalabras(self):
        res = self.redisdb.keys('Palabras:*')
        data = []
        for info in res:
            data.append(info.decode('utf-8'))
        result=[]
        for info in data:
            result.append(self.getByKey(info))
        print(result)

    def updatePalabra(self,palabra,newPalabra):
        data = {'palabra':newPalabra}
        self.redisdb.hmset('Palabras:'+palabra,data)
        print('La palabra se actualizado exitosamente')

    def updateDefinicion(self,palabra,definicion):
        data = {'definicion':definicion}
        self.redisdb.hmset('Palabras:'+palabra,data)
        print('La definicion se actualizado exitosamente')

    def delete(self,palabra):
        self.redisdb.delete('Palabras:'+palabra)
        print('La palabra se eliminado exitosamente')
    
    def getByKey(self,key):
        res = self.redisdb.hget(key,'palabra')
        try:
            value = {'key':key[9:],'palabra':res.decode('utf-8')}
            return value
        except:
            return  {'key':key[9:],'palabra':res}

def main(host,port):
    try:
        redis = REDIS(host,port)
        try:
            exit = False
            op = input('1-Agregar palabra  \n2-Actualizar palabra  3-Actualizar definicion  \n4-Lista de palabras  5-Buscar difinicion  \n6-Eliminar palabra   7-SALIR 🏃‍♂️ \n')
            if(str(op) == '1'):
                print('⚠  RES:')
                redis.post(input('introduzca la palabra:'),input('introduzca su definicion:'))
                
            elif(str(op) == '2'):
                print('⚠  RES:')
                redis.updatePalabra(input('introduzca la key que quiera actualizar su palabra:'),input('introduzca la nueva palabra:'))
                
            elif(str(op) == '3'):
                print('⚠  RES:')
                redis.updateDefinicion(input('introduzca la key que quiera actualizar su definicion:'),input('introduzca la nueva definicion:'))
                
            elif(str(op) == '4'):
                print('⚠  RES:')
                redis.getAllPalabras()
                
            elif(str(op) == '5'):
                print('⚠  RES:')
                redis.getDefinicion(input('introduzca la key que quiera buscar la definicion:'))
                
            elif(str(op) == '6'):
                print('⚠  RES:hay veces en la que debe salir para que se apliquen los cambios')
                redis.delete(input('introduzca la palabra que quieras borrar:'))
            
            elif(str(op) == '7'):
                exit = True

        except Exception as err:
            print('⚠ algo salio mal:')
            print(err)
        
        if(not exit):
            main(host,port)
        
        print('⚠  RES:Saliendo')
            
    except Exception as err:
            print('algo salio mal:')
            print(err)
    


if __name__ == "__main__":
    print('\nBIENCENIDO AL DICCIONARIO DE SLANG PANAMEÑO ✨\n')
    print('nota:"host por defecto:localhost el puerto por defecto:6379"')
    main(input('Itroduzca el host:'),input('Itroduzca el puerto:'),)
